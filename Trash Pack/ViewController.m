//
//  ViewController.m
//  Trash Pack
//
//  Created by Beau Young on 24/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "ViewController.h"
#import "SplashScreen.h"
#import "StoryTitlePage.h"
#import "BoringStuffScene.h"
#import "MenuScreen.h"
#import "Page1.h"
#import "Page12Trash.h"

@implementation ViewController

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    if (!skView.scene) {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        // Create and configure the scene.
        SKScene *scene = [SplashScreen sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
