//
//  MyTrashiesScene.m
//  Trash Pack
//
//  Created by Beau Young on 30/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "MyTrashiesScene.h"
#import "MenuScreen.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


@implementation MyTrashiesScene{
    SKTextureAtlas *textures;
}

-(void)didMoveToView:(SKView *)view {
    // Load Textures
    textures = [self textureAtlasNamed:@"myTrashies"];
    [self createSceneContents];
    self.contentCreated = YES;
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;
    
    // add nav bar
    [self addChild:[self navigationBarWithPreviousButton:NO homeButton:YES forwardButton:NO]];
    
    // Add background
    [self addChild:[self backgroundNode]];
}

#pragma mark - Background Creation
- (SKSpriteNode *)backgroundNode {
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bg"]]; // will change
    backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    return backgroundImage;
}


#pragma mark - Touch Event Listeners
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    if ([node.name isEqualToString:@"home"]) {
        node.scale = 1.2;
        [self changeToScene];
    }
}

#pragma mark - Navigation
- (void)changeToScene {
    SKScene *newScene = [[MenuScreen alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:newScene transition:fade];
}

#pragma mark - Screen Size Helper Method
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else {
        fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    }
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}


@end
