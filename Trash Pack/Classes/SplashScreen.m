//
//  MyScene.m
//  Trash Pack
//
//  Created by Beau Young on 24/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "SplashScreen.h"
#import "EnterGameScreen.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation SplashScreen {
    SKTextureAtlas *textures;
}

- (id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        if (!self.contentCreated) {
            // load textures
            textures = [self textureAtlasNamed:@"splashScreen"];
            [self createSceneContents];
            self.contentCreated = YES;
        }
    }
    return self;
}

- (void)createSceneContents{
    // Set the scale mode and background color
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
        
    // add sprites to the scene
    [self addChild:[self backgroundNode]];
    [self runAction:[self playMusic]];
    
    // Fade title after short delay then move onto next scene
    [self runAction:[self removeTitleWithDelay:1.6] completion:^{
        [self nextScene];
    }];
}
// Play Music Action
- (SKAction *)playMusic {
    SKAction *playMusic = [SKAction playSoundFileNamed:@"intro.aiff" waitForCompletion:NO];
    
    return playMusic;
}
// Create Background
- (SKSpriteNode *)backgroundNode {
    SKSpriteNode *backgroundImage = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"titleBackground"]];
    backgroundImage.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    backgroundImage.zPosition = 0;
    backgroundImage.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    backgroundImage.name = @"backgroundImage";
    
    return backgroundImage;
}
// Fade title
- (SKAction *)removeTitleWithDelay:(float)seconds {
    SKAction *delay = [SKAction waitForDuration:seconds];
    SKAction *fadeAway = [SKAction fadeOutWithDuration:0.25];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *sequence = [SKAction sequence:@[delay, fadeAway, remove]];
    
    return sequence;
}

#pragma mark - Navigation 
- (void)nextScene {
    SKScene *enterGameScene = [[EnterGameScreen alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:enterGameScene transition:fade];
}
#pragma mark - Screen Size Helper Method
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad

    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

@end
