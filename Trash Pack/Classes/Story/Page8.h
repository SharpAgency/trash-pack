//
//  Page8.h
//  Trash Pack
//
//  Created by Beau Young on 5/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "TrashPackMasterScene.h"

@interface Page8 : TrashPackMasterScene

@property BOOL readToMe;
@property BOOL contentCreated;

@end
