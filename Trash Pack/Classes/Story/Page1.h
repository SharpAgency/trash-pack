//
//  Page1.h
//  Trash Pack
//
//  Created by Beau Young on 31/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "TrashPackMasterScene.h"


@interface Page1 : TrashPackMasterScene // Navigation Bar is handled in superclass

@property BOOL contentCreated;
@property BOOL readToMe;

@end
