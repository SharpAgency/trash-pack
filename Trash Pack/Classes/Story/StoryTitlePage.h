//
//  StoryTitlePage.h
//  Trash Pack
//
//  Created by Beau Young on 30/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "TrashPackMasterScene.h"


@interface StoryTitlePage : TrashPackMasterScene // Has code for Navigation Bar

@property BOOL contentCreated;

@property BOOL readToMe;


@end
