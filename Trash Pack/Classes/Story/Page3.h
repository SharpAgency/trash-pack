//
//  Page3.h
//  Trash Pack
//
//  Created by Maris on 4/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "TrashPackMasterScene.h"

@interface Page3 : TrashPackMasterScene

@property BOOL contentCreated;
@property BOOL readToMe;

@end
