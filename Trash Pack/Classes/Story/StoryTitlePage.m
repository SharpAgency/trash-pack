//
//  StoryTitlePage.m
//  Trash Pack
//
//  Created by Beau Young on 30/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "StoryTitlePage.h"
#import "MenuScreen.h"
#import "Page1.h"


#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation StoryTitlePage {
    SKTextureAtlas *textures;
}

- (void)didMoveToView:(SKView *)view {
    // Load Textures
    textures = [self textureAtlasNamed:@"storyTitlePage"];
    [self createSceneContents];
    self.contentCreated = YES;
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;
        
    // Add nav bar
    [self addChild:[self navigationBarWithPreviousButton:NO homeButton:YES forwardButton:NO]];
    
    // Add background and buttons
    [self addChild:[self background]];
    [self addChild:[self spotlight]];
    [self addChild:[self title]];
    [self addChild:[self rankenstein]];
    
    [self addChild:[self readToMeNode]];
    [self addChild:[self letMeReadNode]];
}

#pragma mark - Background & Title Creation
- (SKSpriteNode *)background {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bg"]];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    background.zPosition = 0;
    
    return background;
}
- (SKSpriteNode *)title {
    SKSpriteNode *title = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"title"]];
    title.scale = 0.9;
    title.position = CGPointMake(title.frame.size.width/2, self.frame.size.height-(title.frame.size.height/2));
    title.zPosition = 3;
    
    return title;
}
- (SKSpriteNode *)spotlight{
    SKSpriteNode *spotLight = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"glow"]];
    spotLight.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    spotLight.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    spotLight.zPosition = 2;
    
    return spotLight;
}

#pragma mark - Button Creation
- (SKSpriteNode *)readToMeNode {
    SKSpriteNode *readToMe = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"readToMe"]];
    readToMe.zPosition = 3;
    readToMe.position = CGPointMake(readToMe.frame.size.width/2, readToMe.frame.size.height/2);
    readToMe.name = @"readToMe";
    
    // scale action
    SKAction *scale = [SKAction scaleTo:1.1 duration:1];
    scale.timingMode = SKActionTimingEaseInEaseOut;
    SKAction *downscale = [SKAction scaleTo:0.9 duration:1];
    downscale.timingMode = SKActionTimingEaseInEaseOut;
    SKAction *scaleSequence = [SKAction sequence:@[scale, downscale]];
    
    // apply actions
    [readToMe runAction:[SKAction repeatActionForever:scaleSequence]];
    
    return readToMe;
}

- (SKSpriteNode *)letMeReadNode {
    SKSpriteNode *letMeRead = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"letMeRead"]];
    letMeRead.zPosition = 3;
    letMeRead.position = CGPointMake(letMeRead.frame.size.width+(letMeRead.frame.size.width/2), letMeRead.frame.size.height/2);
    letMeRead.name = @"letMeRead";
    
    // scale action
    SKAction *scale = [SKAction scaleTo:1.1 duration:1];
    scale.timingMode = SKActionTimingEaseInEaseOut;
    SKAction *downscale = [SKAction scaleTo:0.9 duration:1];
    downscale.timingMode = SKActionTimingEaseInEaseOut;
    SKAction *scaleSequence = [SKAction sequence:@[scale, downscale]];
    
    // apply actions
    [letMeRead runAction:[SKAction repeatActionForever:scaleSequence]];
    
    return letMeRead;
}

- (SKSpriteNode *)audioMode {
    SKSpriteNode *audioMode = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"audioMode"]];
    audioMode.zPosition = 3;
    
    return audioMode;
}

#pragma mark - Character Creation
- (SKNode *)rankenstein {
    // Create nodes
    SKNode *rankenstein = [[SKNode alloc] init];
    SKSpriteNode *topOfHead = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"rankentop"]];
    SKSpriteNode *body = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"rankenbody"]];

    // create animation actions
    SKAction *rotateLeft = [SKAction rotateToAngle:0.04 duration:.1 shortestUnitArc:YES];
    SKAction *rotateRight = [SKAction rotateToAngle:6.26 duration:.1 shortestUnitArc:YES];
    SKAction *wiggleSequence = [SKAction sequence:@[rotateLeft, rotateRight]];
    
    // Node setup
    rankenstein.position = CGPointMake(self.frame.size.width-(body.frame.size.width+20), -(body.frame.size.height/4));
    rankenstein.zPosition = 1;
//    rankenstein.scale = 1.2;

    
    // Body
    body.position = CGPointMake(body.frame.size.width/2, (body.frame.size.height/2)-20);
    [body runAction:[SKAction repeatActionForever:wiggleSequence]];
    
    // Top    perhaps make it shoot up at the start
    topOfHead.position = CGPointMake(body.position.x, body.position.y+(body.frame.size.height/2)+(topOfHead.frame.size.height/2)-5);
    [topOfHead runAction:[SKAction repeatActionForever:wiggleSequence]];

    
    [rankenstein addChild:topOfHead];
    [rankenstein addChild:body];
    
    [rankenstein calculateAccumulatedFrame];

    return rankenstein;
}

//#pragma mark - Particle Creation
//- (SKEmitterNode *)steamParticle {
// 
//    
//    
//}

#pragma mark - Touch Event Listeners
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // Nav Bar
    if ([node.name isEqualToString:@"home"]) {
        node.scale = 1.2;
        [self changeToScene:[MenuScreen class]];
    }
    
    // Read Preference selection
    if ([node.name isEqualToString:@"letMeRead"]) {
        node.scale = 1.2;
        self.readToMe = NO;
        [self changeToScene:[Page1 class]];
    }
    
    if ([node.name isEqualToString:@"readToMe"]) {
        node.scale = 1.2;
        self.readToMe = YES;
        [self changeToScene:[Page1 class]];
    }
}

#pragma mark - Navigation
- (void)changeToScene:(Class)scene {
    if ([scene class] == [Page1 class]) {
        Page1 *newScene = [[Page1 alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    else {
        SKScene *newScene = [[scene alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

@end
