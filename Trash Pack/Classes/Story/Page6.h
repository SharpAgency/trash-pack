//
//  Page6.h
//  Trash Pack
//
//  Created by Maris on 4/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "TrashPackMasterScene.h"

@interface Page6 : TrashPackMasterScene

@property BOOL readToMe;
@property BOOL contentCreated;

@end
