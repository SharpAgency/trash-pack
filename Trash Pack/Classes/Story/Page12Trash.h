//
//  Page12Trash.h
//  Trash Pack
//
//  Created by Beau Young on 5/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "TrashPackMasterScene.h"
#import <AVFoundation/AVFoundation.h>


@interface Page12Trash : TrashPackMasterScene <SKPhysicsContactDelegate>

@property BOOL readToMe;
@property BOOL contentCreated;
@property (nonatomic, strong) SKNode *selectedNode;

// Big stuff
@property (strong, nonatomic) SKSpriteNode *trash;
@property (strong, nonatomic) SKSpriteNode *rankenPart;
@property (strong, nonatomic) SKNode *rankenNode;

@property (strong, nonatomic) SKSpriteNode *binArea;
@property (strong, nonatomic) SKSpriteNode *conveyor;

@property (strong, nonatomic) SKSpriteNode *boltArea;
@property (strong, nonatomic) SKSpriteNode *earArea;
@property (strong, nonatomic) SKSpriteNode *eyeArea;
@property (strong, nonatomic) SKSpriteNode *hairArea;
@property (strong, nonatomic) SKSpriteNode *laceArea;
@property (strong, nonatomic) SKSpriteNode *footArea;
@property (strong, nonatomic) SKSpriteNode *mouthArea;
@property (strong, nonatomic) SKSpriteNode *pantsArea;
@property (strong, nonatomic) SKSpriteNode *armArea;
@property (strong, nonatomic) SKSpriteNode *headArea;
@property (strong, nonatomic) SKSpriteNode *torsoArea;

@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) SKAction *conveyorSound;
@property (strong, nonatomic) SKAction *music;

@end
