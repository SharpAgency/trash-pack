//
//  Page23Clips.m
//  Trash Pack
//
//  Created by Beau Young on 5/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "Page22.h"
#import "Page23Clips.h"
#import "Page24.h"

#import "MenuScreen.h"

#define NEXT_SCENE Page24
#define PREV_SCENE Page22

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation Page23Clips {
    SKTextureAtlas *textures;
}

- (void)didMoveToView:(SKView *)view {
    // Load Textures
    textures = [self textureAtlasNamed:@"page23Clamps"];
    [self createSceneContents];
    self.contentCreated = YES;
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;
    
    // Add nav bar
    [self addChild:[self navigationBarWithPreviousButton:YES homeButton:YES forwardButton:YES]];
    
    // Add background and buttons
    [self addChild:[self backgroundNode]];
    [self addChild:[self textBoxNode]];
}
#pragma mark - Scene Contents
- (SKSpriteNode *)backgroundNode {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bg"]];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    background.zPosition = 0;
    
    return background;
}
- (SKSpriteNode *)textBoxNode {
    SKSpriteNode *textBox = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"textPanel"]];
    textBox.position = CGPointMake(CGRectGetMidX(self.frame), 0);
    
    return textBox;
}

#pragma mark - Touch Event Listeners
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // For Nav Bar
    if ([node.name isEqualToString:@"home"]) {
        node.scale = 1.2;
        [self changeToScene:[MenuScreen class]];
    }
    
    if ([node.name isEqualToString:@"prev"]) {
        node.scale = 1.2;
        [self changeToScene:[PREV_SCENE class]];
    }
    
    if ([node.name isEqualToString:@"next"]) {
        node.scale = 1.2;
        [self changeToScene:[NEXT_SCENE class]];
    }
}

#pragma mark - Navigation
- (void)changeToScene:(Class)scene {
    // Prev
    if ([scene class] == [PREV_SCENE class]) {
        PREV_SCENE *newScene = [[PREV_SCENE alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    // Next
    else if ([scene class] == [NEXT_SCENE class]) {
        NEXT_SCENE *newScene = [[NEXT_SCENE alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    // Menu
    else if ([scene class] == [MenuScreen class]) {
        SKScene *newScene = [[scene alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}
@end
