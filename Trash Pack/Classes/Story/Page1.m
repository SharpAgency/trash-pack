//
//  Page1.m
//  Trash Pack
//
//  Created by Beau Young on 31/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "Page1.h"
#import "Page2.h"
#import "MenuScreen.h"
#import "StoryTitlePage.h"

#import "TrashRat.h"
#import "OffCheese.h"
#import "StinkySkunk.h"
#import "PoopMonster.h"

#define NEXT_SCENE Page2
#define PREV_SCENE StoryTitlePage

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation Page1 {
    SKTextureAtlas *textures;
    
    TrashRat *trashRat;
    OffCheese *offCheese;
    PoopMonster *poopMonster;
    StinkySkunk *stinkySkunk;
    
    CGPoint characterPos;
}

- (void)didMoveToView:(SKView *)view {
    // Load Textures
    textures = [self textureAtlasNamed:@"page1"];
    [self createSceneContents];
    self.contentCreated = YES;
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;
    characterPos = CGPointMake(0, self.frame.size.height*0.2);
    
    // Add nav bar
    [self addChild:[self navigationBarWithPreviousButton:YES homeButton:YES forwardButton:YES]];
    
    // Add background and buttons
    [self addChild:[self backgroundNode]];
    [self addChild:[self textBoxNode]];
    
    // Add characters
    [self addChild:[self trashRat]];
    [self addChild:[self poopMonster]];
    [self addChild:[self offCheese]];
    [self addChild:[self stinkySkunk]];
    
    [trashRat runAction:[SKAction repeatActionForever:[SKAction group:@[[self moveUpAndDown], [self moveFoward]]]]];
    [offCheese runAction:[SKAction repeatActionForever:[SKAction group:@[[self moveUpAndDown], [self moveFoward]]]]];
    [poopMonster runAction:[SKAction repeatActionForever:[SKAction group:@[[self moveUpAndDown], [self moveFoward]]]]];
    [stinkySkunk runAction:[SKAction repeatActionForever:[SKAction group:@[[self moveUpAndDown], [self moveFoward]]]]];
}

#pragma mark - Scene Contents
- (SKSpriteNode *)backgroundNode {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bg"]];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    background.zPosition = 0;
    
    return background;
}
- (SKSpriteNode *)textBoxNode {
    SKSpriteNode *textBox = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"textPanel"]];
    textBox.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height-(textBox.frame.size.height*.9));
    
    return textBox;
}

#pragma mark - Character Movement
- (SKAction *)moveUpAndDown {
    SKAction *moveUp = [SKAction moveToY:characterPos.y + 10 duration:0.2];
    SKAction *moveDown = [SKAction moveToY:characterPos.y - 7 duration:0.2];
    SKAction *sequence = [SKAction sequence:@[moveUp, moveDown]];
    
    return sequence;
}

- (SKAction *)moveFoward {
    SKAction *moveForward = [SKAction moveByX:25 y:0 duration:0.2];
    
    return moveForward;
}

#pragma mark - Character Setup
- (TrashRat *)trashRat {
    trashRat = [[TrashRat alloc] init];
    [trashRat isFacingLeft:NO];
    trashRat.position = CGPointMake(-trashRat.frame.size.width/2, characterPos.y);
    return trashRat;
}

- (OffCheese *)offCheese {
    offCheese = [[OffCheese alloc] init];
    [offCheese isFacingLeft:NO];
    offCheese.position = CGPointMake(trashRat.position.x-300, characterPos.y);

    return offCheese;
}

- (PoopMonster *)poopMonster {
    poopMonster = [[PoopMonster alloc] init];
    [poopMonster isFacingLeft:NO];
    poopMonster.position = CGPointMake(offCheese.position.x-300, characterPos.y);

    return poopMonster;
}

- (StinkySkunk *)stinkySkunk {
    stinkySkunk = [[StinkySkunk alloc] init];
    [stinkySkunk isFacingLeft:NO];
    stinkySkunk.position = CGPointMake(poopMonster.position.x-300, characterPos.y);

    return stinkySkunk;
}

#pragma mark - Touch Event Listeners
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // For Nav Bar
    if ([node.name isEqualToString:@"home"]) {
        node.scale = 1.2;
        [self changeToScene:[MenuScreen class]];
    }
    if ([node.name isEqualToString:@"prev"]) {
        node.scale = 1.2;
        [self changeToScene:[PREV_SCENE class]];
    }
    if ([node.name isEqualToString:@"next"]) {
        node.scale = 1.2;
        [self changeToScene:[NEXT_SCENE class]];
    }
}

#pragma mark - Navigation
- (void)changeToScene:(Class)scene {
    // Prev
    if ([scene class] == [PREV_SCENE class]) {
        PREV_SCENE *newScene = [[PREV_SCENE alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    // Next
    else if ([scene class] == [NEXT_SCENE class]) {
        NEXT_SCENE *newScene = [[NEXT_SCENE alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    // Menu
    else if ([scene class] == [MenuScreen class]) {
        SKScene *newScene = [[scene alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

#pragma mark - Update Method
- (void)update:(NSTimeInterval)currentTime {
    
    
    
}


@end
