//
//  Page12Trash.m
//  Trash Pack
//
//  Created by Beau Young on 5/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "Page11.h"
#import "Page12Trash.h"
#import "Page13.h"

#import "MenuScreen.h"

#define NEXT_SCENE Page13
#define PREV_SCENE Page11

#define CONVEYOR_TOP CGRectGetMidY(self.frame)+70
#define CONVEYOR_BOTTOM CGRectGetMidY(self.frame)-70

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation Page12Trash {
    SKTextureAtlas *textures;
    
    NSArray *trashNameArray;
    NSMutableArray *rankensteinNameArray;
    
    BOOL gameComplete;
}

- (void)didMoveToView:(SKView *)view {
    // Load Textures
    textures = [self textureAtlasNamed:@"page12Conveyor"];
    self.conveyorSound = [SKAction playSoundFileNamed:@"factory.aif" waitForCompletion:YES];   /// preload other sounds too
    [self runAction:[SKAction repeatActionForever:_conveyorSound]];
    
    gameComplete = NO;
    [self createSceneContents];
    self.contentCreated = YES;
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;
    
    // Add nav bar
    [self addChild:[self navigationBarWithPreviousButton:YES homeButton:YES forwardButton:YES]];
    
    // Add background
    [self addChild:[self backgroundNode]];
    
    // Populate Trash name Array
    trashNameArray = @[@"bluerag",
                   @"bread",
                   @"bucket",
                   @"bumper",
                   @"fan",
                   @"garbagebag",
                   @"glass",
                   @"glob",
                   @"gum",
                   @"jar",
                   @"macaroni",
                   @"milk",
                   @"nappy",
                   @"paintcan",
                   @"phone",
                   @"pipe",
                   @"ribs",
                   @"rim",
                   @"runner",
                   @"slurpee",
                   @"spring",
                   @"steelring",
                   @"teeth",
                   @"toiletpaper",
                   @"tyre",
                   @"waterbottle"];
    
    // populate rankenstein name array
    rankensteinNameArray = [NSMutableArray arrayWithObjects:@"rank_bolt",
                                                            @"rank_ear",
                                                            @"rank_eyeball1",
                                                            @"rank_hair",
                                                            @"rank_lace1",
                                                            @"rank_leftfoot",
                                                            @"rank_mouth",
                                                            @"rank_pants",
                                                            @"rank_rightarm",
                                                            @"rank_tophead",
                                                            @"rank_torso", nil];
    
    
    SKAction *makeTrash = [SKAction sequence:@[
                                               [SKAction performSelector:@selector(addTrash) onTarget:self],
                                               [SKAction waitForDuration:0.5 withRange:0.15]
                                               ]];
    [self runAction:[SKAction repeatActionForever:makeTrash]];
    
    
    SKAction *makeRankenParts = [SKAction sequence:@[
                                                     [SKAction performSelector:@selector(addRankenParts) onTarget:self],
                                                     [SKAction waitForDuration:4 withRange:.5]
                                                     ]];
    [self runAction:[SKAction repeatActionForever:makeRankenParts]];
    

    // Drop Zones
    _boltArea = [self rankBolt];
    _boltArea.position = CGPointMake(self.size.width*.299, self.size.height*.129);
    [self addChild:_boltArea];
    
    _earArea = [self rankEar];
    _earArea.position = CGPointMake(self.size.width*.08, self.size.height*.064);
    [self addChild:_earArea];
    
    _eyeArea = [self rankEyeball];
    _eyeArea.position = CGPointMake(self.size.width*.542, self.size.height*.179);
    [self addChild:_eyeArea];
    
    _hairArea = [self rankhair];
    _hairArea.position = CGPointMake(self.size.width*.26, self.size.height*.186);
    [self addChild:_hairArea];
    
    _laceArea = [self rankLace];
    _laceArea.position = CGPointMake(self.size.width*.42, self.size.height*.115);
    [self addChild:_laceArea];
    
    _footArea = [self rankLeftfoot];
    _footArea.position = CGPointMake(self.size.width*.439, self.size.height*.051);
    [self addChild:_footArea];
    
    _mouthArea = [self rankMouth];
    _mouthArea.position = CGPointMake(self.size.width*.54, self.size.height*.067);
    [self addChild:_mouthArea];
    
    _pantsArea = [self rankPants];
    _pantsArea.position = CGPointMake(self.size.width*.217, self.size.height*.056);
    [self addChild:_pantsArea];
    
    _armArea = [self rankArm];
    _armArea.position = CGPointMake(self.size.width*.341, self.size.height*.058);
    [self addChild:_armArea];
    
    _headArea = [self rankHead];
    _headArea.position = CGPointMake(self.size.width*.406, self.size.height*.182);
    [self addChild:_headArea];
    
    _torsoArea = [self rankTorso];
    _torsoArea.position = CGPointMake(self.size.width*.107, self.size.height*.176);
    [self addChild:_torsoArea];
    
    SKSpriteNode *bin = [self bin];
    bin.position = CGPointMake(self.size.width-(bin.size.width/2), 0);
    [self addChild:bin];
    
    [self conveyorSection];
}
#pragma mark - Scene Contents
// For random placement of trash spawn
static inline CGFloat skRandf() {
    return rand() / (CGFloat) RAND_MAX;
}
static inline CGFloat skRand(CGFloat low, CGFloat high) {
    return skRandf() * (high - low) + low;
}

// collision detection categories, each category is assigned to a bit. Max of 8 bits in this case.
static const uint8_t trashCategory = 1;
static const uint8_t binCategory = 2;
static const uint8_t conveyorCategory = 3;

- (SKSpriteNode *)backgroundNode {
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bg"]];
    background.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    background.size = CGSizeMake(background.size.width, self.frame.size.height);
    background.zPosition = 0;
    
    return background;
}
- (void)conveyorSection {
    for (int i = 0; i < 2; i++) {
        self.conveyor = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"conveyorsection"]];
        self.conveyor.anchorPoint = CGPointZero;
        self.conveyor.position = CGPointMake(i * self.conveyor.size.width, self.frame.size.height*.285);
        self.conveyor.name = @"background";
        [self addChild:self.conveyor];
    }
}
- (SKSpriteNode *)bin {
    self.binArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(self.size.width*.38, self.size.width*.4)];
    self.binArea.name = @"bin";
    
    return self.binArea;
}
- (void)addTrash {
    float trashArrayCount = [trashNameArray count];
    int i = arc4random_uniform(trashArrayCount);
    
    _trash = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:[trashNameArray objectAtIndex:i]]];
    _trash.position = CGPointMake(self.size.width+250, skRand(CONVEYOR_TOP, CONVEYOR_BOTTOM));
    _trash.name = @"trash";
    
    [self addChild:self.trash];
}
- (void)addRankenParts {
    if ([rankensteinNameArray count]) {
        float rankenPartsCount = [rankensteinNameArray count];
        int i = arc4random_uniform(rankenPartsCount);
        
        _rankenPart = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:[rankensteinNameArray objectAtIndex:i]]];
        _rankenPart.position = CGPointMake(self.size.width+250, skRand(CONVEYOR_TOP, CONVEYOR_BOTTOM));
        _rankenPart.name = [rankensteinNameArray objectAtIndex:i];
        
        [self addChild:_rankenPart];
        
    } else if
        (![rankensteinNameArray count]) {
            gameComplete = YES;
            if (gameComplete) {
                gameComplete = NO;
                [self showWinText];
            }
        }
}

// Drop Zones
- (SKSpriteNode *)rankBolt {
    SKSpriteNode *boltArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(40, 40)];
    boltArea.name = @"rank_bolt_area";
    return boltArea;
}
- (SKSpriteNode *)rankEar {
    SKSpriteNode *earArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(60, 40)];
    earArea.name = @"rank_ear_area";
    return earArea;
}
- (SKSpriteNode *)rankEyeball {
    SKSpriteNode *eyeArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(55, 55)];
    eyeArea.name = @"rank_eyeball1_area";
    return eyeArea;
}
- (SKSpriteNode *)rankhair {
    SKSpriteNode *hairArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(125, 45)];
    hairArea.name = @"rank_hair_area";
    return hairArea;
}
- (SKSpriteNode *)rankLace {
    SKSpriteNode *laceArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(80, 30)];
    laceArea.name = @"rank_lace1_area";
    return laceArea;
}
- (SKSpriteNode *)rankLeftfoot {
    SKSpriteNode *footArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(90, 50)];
    footArea.name = @"rank_leftfoot_area";
    return footArea;
}
- (SKSpriteNode *)rankMouth {
    SKSpriteNode *mouthArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(70, 85)];
    mouthArea.name = @"rank_mouth_area";
    return mouthArea;
}
- (SKSpriteNode *)rankPants {
    SKSpriteNode *pantsArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(130, 60)];
    pantsArea.name = @"rank_pants_area";
    return pantsArea;
}
- (SKSpriteNode *)rankArm {
    SKSpriteNode *armArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(95, 60)];
    armArea.name = @"rank_rightarm_area";
    return armArea;
}
- (SKSpriteNode *)rankHead {
    SKSpriteNode *headArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(125, 50)];
    headArea.name = @"rank_tophead_area";
    return headArea;
}
- (SKSpriteNode *)rankTorso {
    SKSpriteNode *torsoArea = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(125, 80)];
    torsoArea.name = @"rank_torso_area";
    return torsoArea;
}

#pragma mark - Update Method
- (void)update:(NSTimeInterval)currentTime {
    // Scroll conveyor sprite
    [self enumerateChildNodesWithName:@"background" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *bg = (SKSpriteNode *) node;
        bg.position = CGPointMake(bg.position.x - 2, bg.position.y);
        
        if (bg.position.x <= -bg.size.width) {
            bg.position = CGPointMake(bg.position.x + bg.size.width * 2, bg.position.y);
        }
    }];
    
    // move trash every frame
    [self enumerateChildNodesWithName:@"trash" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    
    // For placing selected trash back on conveyor
    if ([[_selectedNode name] isEqualToString:kAnimalNodeName]) {
        if (_selectedNode.position.y <= CONVEYOR_TOP && _selectedNode.position.y >= CONVEYOR_BOTTOM) {
            self.selectedNode.position = CGPointMake(self.selectedNode.position.x - 2, self.selectedNode.position.y);
        }
    }
    
    // For ranken parts on conveyor
    [self enumerateChildNodesWithName:@"rank_bolt" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_ear" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_eyeball1" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_hair" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_lace1" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_leftfoot" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_mouth" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_pants" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_rightarm" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_tophead" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
    [self enumerateChildNodesWithName:@"rank_torso" usingBlock: ^(SKNode *node, BOOL *stop) {
        SKSpriteNode *trash = (SKSpriteNode *) node;
        trash.position = CGPointMake(trash.position.x - 2, trash.position.y);
    }];
}

#pragma mark - Touch Event Listeners
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    // For Nav Bar
    if ([node.name isEqualToString:@"home"]) {
        node.scale = 1.2;
        [self removeAllActions];
        [self changeToScene:[MenuScreen class]];
    }
    if ([node.name isEqualToString:@"prev"]) {
        node.scale = 1.2;
        [self removeAllActions];
        [self changeToScene:[PREV_SCENE class]];
    }
    if ([node.name isEqualToString:@"next"]) {
        node.scale = 1.2;
        [self removeAllActions];
        [self changeToScene:[NEXT_SCENE class]];
    }
    
    [self selectNodeForTouch:location];
}

static NSString * const kAnimalNodeName = @"pickedTrash";

float degToRad(float degree) {
	return degree / 180.0f * M_PI;
}
- (void)selectNodeForTouch:(CGPoint)touchLocation {
    SKNode *touchedNode = (SKNode *)[self nodeAtPoint:touchLocation];
    
	if(![_selectedNode isEqual:touchedNode]) {
		[_selectedNode removeAllActions];
        _selectedNode = touchedNode;

		if([[touchedNode name] isEqualToString:@"trash"]) {
            touchedNode.name = kAnimalNodeName;
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        
        if([[touchedNode name] isEqualToString:@"rank_bolt"]) {
            touchedNode.name = @"rank_bolt_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_ear"]) {
            touchedNode.name = @"rank_ear_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_eyeball1"]) {
            touchedNode.name = @"rank_eyeball1_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_hair"]) {
            touchedNode.name = @"rank_hair_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_lace1"]) {
            touchedNode.name = @"rank_lace1_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_leftfoot"]) {
            touchedNode.name = @"rank_leftfoot_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_mouth"]) {
            touchedNode.name = @"rank_mouth_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_pants"]) {
            touchedNode.name = @"rank_pants_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_rightarm"]) {
            touchedNode.name = @"rank_rightarm_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_tophead"]) {
            touchedNode.name = @"rank_tophead_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
        if([[touchedNode name] isEqualToString:@"rank_torso"]) {
            touchedNode.name = @"rank_torso_selected";
            _selectedNode.zPosition = 25;
            [self runAction:[SKAction playSoundFileNamed:@"pickUpTrash.caf" waitForCompletion:NO]];
		}
	}
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	UITouch *touch = [touches anyObject];
	CGPoint positionInScene = [touch locationInNode:self];
	CGPoint previousPosition = [touch previousLocationInNode:self];
    
	CGPoint translation = CGPointMake(positionInScene.x - previousPosition.x, positionInScene.y - previousPosition.y);
	[self panForTranslation:translation];
}
- (void)panForTranslation:(CGPoint)translation {
    CGPoint position = [_selectedNode position];
    
    if([[_selectedNode name] isEqualToString:kAnimalNodeName]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }

    if([[_selectedNode name] isEqualToString:@"rank_bolt_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_ear_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_eyeball1_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_hair_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_lace1_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_leftfoot_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_mouth_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_pants_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_rightarm_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_tophead_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
    if([[_selectedNode name] isEqualToString:@"rank_torso_selected"]) {
        [_selectedNode setPosition:CGPointMake(position.x + translation.x, position.y + translation.y)];
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(_binArea.frame, _selectedNode.position)) {
        [self.selectedNode removeFromParent];
        int n = arc4random_uniform(3);
        if (n == 0) {
            [self runAction:[SKAction playSoundFileNamed:@"bin1.caf" waitForCompletion:NO]];
        }
        if (n == 1) {
            [self runAction:[SKAction playSoundFileNamed:@"bin2.caf" waitForCompletion:NO]];
        }
        if (n == 2) {
            [self runAction:[SKAction playSoundFileNamed:@"bin3.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_boltArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_bolt_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _boltArea.position;
            [rankensteinNameArray removeObject:@"rank_bolt"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_earArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_ear_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _earArea.position;
            [rankensteinNameArray removeObject:@"rank_ear"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_eyeArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_eyeball1_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _eyeArea.position;
            [rankensteinNameArray removeObject:@"rank_eyeball1"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_hairArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_hair_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _hairArea.position;
            [rankensteinNameArray removeObject:@"rank_hair"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_laceArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_lace1_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _laceArea.position;
            [rankensteinNameArray removeObject:@"rank_lace1"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_footArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_leftfoot_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _footArea.position;
            [rankensteinNameArray removeObject:@"rank_leftfoot"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_mouthArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_mouth_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _mouthArea.position;
            [rankensteinNameArray removeObject:@"rank_mouth"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_pantsArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_pants_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _pantsArea.position;
            [rankensteinNameArray removeObject:@"rank_pants"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_armArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_rightarm_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _armArea.position;
            [rankensteinNameArray removeObject:@"rank_rightarm"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_headArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_tophead_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _headArea.position;
            [rankensteinNameArray removeObject:@"rank_tophead"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
    if (CGRectContainsPoint(_torsoArea.frame, _selectedNode.position)) {
        if ([_selectedNode.name isEqualToString:@"rank_torso_selected"]) {
            _selectedNode.name = @"placed";
            _selectedNode.position = _torsoArea.position;
            [rankensteinNameArray removeObject:@"rank_torso"];
            [self runAction:[SKAction playSoundFileNamed:@"trashDown.caf" waitForCompletion:NO]];
        }
    }
}

#pragma mark - Navigation
- (void)changeToScene:(Class)scene {
    // Prev
    if ([scene class] == [PREV_SCENE class]) {
        PREV_SCENE *newScene = [[PREV_SCENE alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    // Next
    else if ([scene class] == [NEXT_SCENE class]) {
        NEXT_SCENE *newScene = [[NEXT_SCENE alloc] initWithSize:self.size];
        newScene.readToMe = self.readToMe;
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
    // Menu
    else if ([scene class] == [MenuScreen class]) {
        SKScene *newScene = [[scene alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
    }
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

#pragma mark - end game method
- (void)showWinText {
    SKLabelNode *winLabel = [SKLabelNode labelNodeWithFontNamed:@"ScooteramaBold"];
    winLabel.text = @"Task Complete";
    winLabel.fontSize = 100;
    winLabel.fontColor = [SKColor whiteColor];
    winLabel.zPosition = 30;
    winLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    SKAction *moveUp = [SKAction moveByX:0 y:100 duration:0.8];
    SKAction *fade = [SKAction fadeOutWithDuration:1];
    
    SKAction *sequence = [SKAction group:@[moveUp, fade]];
    
    [self addChild:winLabel];
    
    [winLabel runAction:sequence completion:^{
            NEXT_SCENE *newScene = [[NEXT_SCENE alloc] initWithSize:self.size];
            newScene.readToMe = self.readToMe;
            SKTransition *fade = [SKTransition fadeWithDuration:0.5];
            [self.view presentScene:newScene transition:fade];
    }];
}

@end
