//
//  TrashPackMasterScene.m
//  Trash Pack
//
//  Created by Beau Young on 2/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "TrashPackMasterScene.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define NAV_TEXT_IPHONE 15
#define NAV_TEXT_IPAD 35

@implementation TrashPackMasterScene {
    SKTextureAtlas *textures;
    int navigationTextSize;
    int navbarYposition;
}

#pragma mark - Navigation Bar
- (SKNode *)navigationBarWithPreviousButton:(BOOL)previousBtn homeButton:(BOOL)homeBtn forwardButton:(BOOL)forwardBtn {
    textures = [self navigationTextureAtlasNamed:@"navigationMenu"];
    SKNode *navigationBar = [[SKNode alloc] init];
    
    // Check if certain buttons are wanted, if so excecute methods to create them
    if (previousBtn) {
        [navigationBar addChild:[self previousButton]];
    }
    if (homeBtn) {
        [navigationBar addChild:[self homeButton]];
    }
    if (forwardBtn) {
        [navigationBar addChild:[self forwardButton]];
    }
    [navigationBar calculateAccumulatedFrame];
    navigationBar.position = CGPointMake(0, self.frame.size.height-navbarYposition);
    navigationBar.zPosition = 100;
    
    return navigationBar;
}
// Methods for creating Navigation Bar content
    // Create Previous Button
- (SKSpriteNode *)previousButton {
    SKSpriteNode *previousBtn = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"prev"]];
    previousBtn.position = CGPointMake((previousBtn.frame.size.width/2)+5, previousBtn.frame.size.height/2);
    previousBtn.name = @"prev";
    return previousBtn;
}
    // Create Home Button
- (SKSpriteNode *)homeButton {
    SKSpriteNode *homeBtn = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"home"]];
    homeBtn.position = CGPointMake(homeBtn.frame.size.width*2, homeBtn.frame.size.height/2);
    
    // Add text
    SKLabelNode *homeText = [SKLabelNode labelNodeWithFontNamed:@"ScooteramaBold"];
    homeText.text = @"Home";
    homeText.fontSize = navigationTextSize; // Variable size, dependent on device type
    homeText.position = CGPointMake(homeBtn.frame.size.width, -homeText.frame.size.height/2);
    homeText.fontColor = [SKColor whiteColor];
    [homeText setUserInteractionEnabled:YES];
    
    homeBtn.name = @"home";
    homeText.name = @"home";
    
    [homeBtn addChild:homeText];

    return homeBtn;
}
    // Create Forward Button
- (SKSpriteNode *)forwardButton {
    SKSpriteNode *forwardBtn = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"next"]];
    forwardBtn.position = CGPointMake(self.frame.size.width-(forwardBtn.frame.size.width/2)-5, forwardBtn.frame.size.height/2);
    forwardBtn.name = @"next";
    
    return forwardBtn;
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)navigationTextureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        navigationTextSize = NAV_TEXT_IPHONE;
        navbarYposition = 28;
        
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName; // iPhone 3.5 inch
    }
    else {
        navigationTextSize = NAV_TEXT_IPAD;
        navbarYposition = 56;
        fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    }
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];

    return textureAtlas;
}

@end
