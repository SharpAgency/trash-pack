//
//  TrashRat.m
//  Trash Pack
//
//  Created by Maris on 4/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "TrashRat.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation TrashRat {
    SKTextureAtlas *textures;
    SKSpriteNode *character;
}

- (id)init {
    if (self = [super init]) {
        textures = [self textureAtlasNamed:@"trashRat"];
        [self setUpDetails];
        self.userInteractionEnabled = YES;
    }
    return self;
}
- (void)setUpDetails {
    self.name = @"trashRat";
    character = [SKSpriteNode spriteNodeWithImageNamed:@"trashrat_talks1"];
    [self addChild:character];
}

#pragma mark - Movement
- (void)moveAnimation {
    SKAction *scaleYUp = [SKAction scaleYTo:1.05 duration:0.4];
    SKAction *scaleYDown = [SKAction scaleYTo:0.95 duration:0.4];
    SKAction *scaleSequence = [SKAction sequence:@[scaleYUp, scaleYDown]];
    
    [character runAction:[SKAction repeatActionForever:scaleSequence]];
}

- (void)talkAnimation {
    NSArray *talkingTextures = @[[textures textureNamed:@"trashrat_talks1"],
                                 [textures textureNamed:@"trashrat_talks2"]];
    
    SKAction *animation = [SKAction animateWithTextures:talkingTextures timePerFrame:0.2];
    [character runAction:[SKAction repeatActionForever:animation]];
}

#pragma mark - Expressions
- (void)tellJoke {
    // place random joke code in here
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-Light"];
    label.text = @"Joke Told";
    label.fontSize = 35;
    label.fontColor = [SKColor whiteColor];
    label.zPosition = 20;
    
    SKAction *moveUp = [SKAction moveByX:0 y:100 duration:0.8];
    SKAction *fade = [SKAction fadeOutWithDuration:0.8];
    SKAction *remove = [SKAction removeFromParent];
    
    SKAction *sequence = [SKAction group:@[moveUp, fade]];

    [self addChild:label];
    [label runAction:sequence completion:^{
        [label runAction:remove completion:nil];
    }];
    
    NSLog(@"jokeTold");
}

- (void)burp {
    SKAction *burp = [SKAction playSoundFileNamed:[NSString stringWithFormat:@"burp%d.caf", arc4random_uniform(7)+1] waitForCompletion:NO];
    [self runAction:burp];
}

- (void)fart {
    SKAction *fart = [SKAction playSoundFileNamed:[NSString stringWithFormat:@"fart%d.caf", arc4random_uniform(9)+1] waitForCompletion:NO];
    [self runAction:fart];
}

- (void)sneeze {
    SKAction *sneeze = [SKAction playSoundFileNamed:[NSString stringWithFormat:@"sneeze%d.caf", arc4random_uniform(4)+1] waitForCompletion:NO];

    [self runAction:sneeze];
}

- (void)stopAnimating {
    [character removeAllActions];
}

#pragma mark - Direction
- (void)isFacingLeft:(BOOL)left {
    if (left == YES) self.xScale = 1;
    else self.xScale = -1;
}


#pragma mark - Touch Listener
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    UITouch *touch = [touches anyObject];
//    CGPoint locationA = [touch locationInNode:self];
//    CGPoint locationB = [touch locationInNode:self.parent];
//    NSLog(@"Hit at, %@ %@", NSStringFromCGPoint(locationA), NSStringFromCGPoint(locationB));
    
    int method = arc4random_uniform(3);
    if (method == 0) [self tellJoke];
    if (method == 1) [self burp];
    if (method == 2) [self fart];
    if (method == 3) [self sneeze];
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

@end
