//
//  TrashRat.h
//  Trash Pack
//
//  Created by Maris on 4/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface TrashRat : SKNode

- (id)init;

- (void)moveAnimation;
- (void)talkAnimation;

- (void)burp;
- (void)fart;
- (void)sneeze;
- (void)tellJoke;

- (void)stopAnimating;

- (void)isFacingLeft:(BOOL)left;

@end
