//
//  OffCheese.m
//  Trash Pack
//
//  Created by Beau Young on 5/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "OffCheese.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation OffCheese {
    SKTextureAtlas *textures;
    SKSpriteNode *character;
}

- (id)init {
    if (self = [super init]) {
        textures = [self textureAtlasNamed:@"offCheese"];
        [self setUpDetails];
        self.userInteractionEnabled = YES;
    }
    return self;
}
- (void)setUpDetails {
    self.name = @"offCheese";
    character = [SKSpriteNode spriteNodeWithImageNamed:@"OffCheesewalk1"];
    [self addChild:character];
    
}
#pragma mark - Movement
- (void)moveAnimation {
    NSArray *walkingTextures = @[[textures textureNamed:@"OffCheesewalk1"],
                                 [textures textureNamed:@"OffCheesewalk2"]];
    
    SKAction *animation = [SKAction animateWithTextures:walkingTextures timePerFrame:0.2];
    [character runAction:[SKAction repeatActionForever:animation]];
}

- (void)talkAnimation {
    NSArray *talkingTextures = @[[textures textureNamed:@"offCheesetalk1_left"],
                                 [textures textureNamed:@"offCheesetalk2_left"]];
    
    SKAction *animation = [SKAction animateWithTextures:talkingTextures timePerFrame:0.2];
    [character runAction:[SKAction repeatActionForever:animation]];
    
}

#pragma mark - Expressions

- (void)tellJoke {
    // place random joke code in here
    NSLog(@"jokeTold");
}


- (void)burp {
    
}

- (void)fart {
    
}

- (void)sneeze {
    
}

- (void)stopAnimating {
    [character removeAllActions];
}

#pragma mark - Direction
- (void)isFacingLeft:(BOOL)left {
    if (left == YES) self.xScale = 1;
    else self.xScale = -1;
}


#pragma mark - Touch Listener
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self tellJoke];
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

@end
