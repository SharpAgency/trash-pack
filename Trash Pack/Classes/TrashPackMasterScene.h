//
//  TrashPackMasterScene.h
//  Trash Pack
//
//  Created by Beau Young on 2/11/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface TrashPackMasterScene : SKScene

// Creates the navigation bar
- (SKNode *)navigationBarWithPreviousButton:(BOOL)previousBtn homeButton:(BOOL)homeBtn forwardButton:(BOOL)forwardBtn;

@end
