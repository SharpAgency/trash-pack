//
//  MenuScreen.h
//  Trash Pack
//
//  Created by Beau Young on 26/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "TrashPackMasterScene.h"


@interface MenuScreen : TrashPackMasterScene

@property BOOL contentCreated;

@end
