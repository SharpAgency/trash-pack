//
//  MenuScreen.m
//  Trash Pack
//
//  Created by Beau Young on 26/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import "MenuScreen.h"

#import "StoryTitlePage.h"
#import "TVAdvertsScene.h"
#import "MyTrashiesScene.h"
#import "CartoonsScene.h"
#import "BoringStuffScene.h"
#import "SKButtonNode.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation MenuScreen {
    SKTextureAtlas *textures;
}

- (void)didMoveToView:(SKView *)view {
    if (!self.contentCreated) {
        // Load Textures
        textures = [self textureAtlasNamed:@"menuScreen"];
        [self createSceneContents];
        self.contentCreated = YES;
    }
}

- (void)createSceneContents {
    self.scaleMode = SKSceneScaleModeAspectFill;

    // Add background and buttons to scene
    [self addChild:[self backgroundNode]];
    [self addChild:[self logoNode]];
    [self addChild:[self welcomeNode]];
    [self addChild:[self myTrashiesNode]];
    [self addChild:[self cartoonsNode]];
    [self addChild:[self tvAdvertsNode]];
    [self addChild:[self boringStuffNode]];
    [self addChild:[self storyNode]];
    [self addChild:[self bookOneNode]];
    [self addChild:[self bookTwoNode]];
}

#pragma mark - Background and Button Creation Methods
- (SKSpriteNode *)backgroundNode {
    // Create animated background
    SKSpriteNode *backgroundSprite = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"electricMenu3"]];
    backgroundSprite.position = CGPointMake(CGRectGetMidX(self.frame), (CGRectGetMidY(self.frame)));
    backgroundSprite.zPosition = 0;
    NSArray *backgroundTextures = @[[textures textureNamed:@"electricMenu2"],
                                    [textures textureNamed:@"electricMenu"],
                                    [textures textureNamed:@"electricMenu2"],
                                    [textures textureNamed:@"electricMenu3"]];
    SKAction *delayBackground = [SKAction waitForDuration:2];
    SKAction *animateBackground = [SKAction animateWithTextures:backgroundTextures timePerFrame:0.15];
    SKAction *backgroundSequence = [SKAction sequence:@[delayBackground, animateBackground]];
    [backgroundSprite runAction:[SKAction repeatActionForever:backgroundSequence]];
    
    return backgroundSprite;
}
    // Create logo
- (SKSpriteNode *)logoNode {
    SKSpriteNode *logo = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"logo"]];
    logo.position = CGPointMake(logo.frame.size.width/2, self.frame.size.height-logo.frame.size.height/2);
    logo.zPosition = 1;
    
    return logo;
}
    // Create Headline Text
- (SKSpriteNode *)welcomeNode {
    SKSpriteNode *welcome = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"title"]];
    welcome.position = CGPointMake(self.frame.size.width - welcome.frame.size.width/2, (self.frame.size.height-welcome.frame.size.height/2)-5);
    welcome.zPosition = 1;
    
    return welcome;
}
- (SKButtonNode *)myTrashiesNode {
    SKButtonNode *myTrashies = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"myTrashies1"]
                                                                  selected:[textures textureNamed:@"myTrashies2"]];
    
    myTrashies.position = CGPointMake(self.frame.size.width/2 + 20, CGRectGetMidY(self.frame));
    myTrashies.zPosition = 2;
    myTrashies.name = @"MyTrashiesScene"; // This is also the name of the class the button leads to
    [myTrashies setTouchUpInsideTarget:self action:@selector(changeToSceneMyTrashies)];

    return myTrashies;
}
- (SKButtonNode *)cartoonsNode {
    SKButtonNode *cartoons = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"gallery1"]
                                                                selected:[textures textureNamed:@"gallery2"]];
    cartoons.position = CGPointMake(CGRectGetMidX(self.frame)-15, cartoons.frame.size.height/2+5);
    cartoons.zPosition = 2;
    cartoons.name = @"CartoonsScene"; // This is also the name of the class the button leads to
    [cartoons setTouchUpInsideTarget:self action:@selector(changeToSceneCartoons)];
    
    return cartoons;
}
- (SKButtonNode *)tvAdvertsNode {
    SKButtonNode *tvAdverts = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"tvAdverts1"]
                                                                 selected:[textures textureNamed:@"tvAdverts2"]];
    tvAdverts.position = CGPointMake(CGRectGetMidX(self.frame), self.frame.size.height *0.3);
    tvAdverts.zPosition = 2;
    tvAdverts.name = @"TVAdvertsScene"; // This is also the name of the class the button leads to
    [tvAdverts setTouchUpInsideTarget:self action:@selector(changeToSceneTvAdverts)];

    return tvAdverts;
}
- (SKButtonNode *)boringStuffNode {
    SKButtonNode *boringStuff = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"boringStuff1"]
                                                                   selected:[textures textureNamed:@"boringStuff2"]];
    boringStuff.position = CGPointMake(self.frame.size.width-(boringStuff.frame.size.width/2)-15, boringStuff.frame.size.height/2+10);
    boringStuff.zPosition = 2;
    boringStuff.name = @"BoringStuffScene"; // This is also the name of the class the button leads to
    [boringStuff setTouchUpInsideTarget:self action:@selector(changeToSceneBoringStuff)];

    return boringStuff;
}
- (SKButtonNode *)storyNode {
    SKButtonNode *story = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"story1"]
                                                             selected:[textures textureNamed:@"story2"]];
    story.position = CGPointMake(self.frame.size.width-(story.frame.size.width/2)-5, self.frame.size.height*0.4);
    story.zPosition = 2;
    story.name = @"StoryTitlePage"; // This is also the name of the class the button leads to
    [story setTouchUpInsideTarget:self action:@selector(changeToSceneStory)];

    return story;
}
- (SKButtonNode *)bookOneNode {
    SKButtonNode *bookOneIcon = [SKButtonNode spriteNodeWithTexture:[textures textureNamed:@"ibook1"]];
    bookOneIcon.position = CGPointMake((bookOneIcon.frame.size.width/2)+10, (bookOneIcon.frame.size.height/2)+10);
    bookOneIcon.zPosition = 2;
    bookOneIcon.scale = 0.6;

    return bookOneIcon;
}
- (SKButtonNode *)bookTwoNode {
    SKButtonNode *bookTwoIcon = [SKButtonNode spriteNodeWithTexture:[textures textureNamed:@"iBook2"]];
    bookTwoIcon.position = CGPointMake(bookTwoIcon.frame.size.width+30, (bookTwoIcon.frame.size.height/2)+10);
    bookTwoIcon.zPosition = 2;
    bookTwoIcon.scale = 0.6;
    
    return bookTwoIcon;
}
#pragma mark - Navigation
- (void)changeToSceneMyTrashies {
        SKScene *newScene = [[MyTrashiesScene alloc] initWithSize:self.size];
        SKTransition *fade = [SKTransition fadeWithDuration:0.5];
        [self.view presentScene:newScene transition:fade];
}

- (void)changeToSceneCartoons {
    SKScene *newScene = [[CartoonsScene alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:newScene transition:fade];
}

- (void)changeToSceneBoringStuff {
    SKScene *newscene = [[BoringStuffScene alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:newscene transition:fade];
}

- (void)changeToSceneStory {
    SKScene *newScene = [[StoryTitlePage alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:newScene transition:fade];
}

- (void)changeToSceneTvAdverts {
    SKScene *newScene = [[TVAdvertsScene alloc] initWithSize:self.size];
    SKTransition *fade = [SKTransition fadeWithDuration:0.5];
    [self.view presentScene:newScene transition:fade];
}

#pragma mark - Screen Size Helper Method for choosing textureAtlas
- (SKTextureAtlas *)textureAtlasNamed:(NSString *)fileName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) fileName = [NSString stringWithFormat:@"%@-568", fileName]; // iPhone Retina 4-inch
        else fileName = fileName;                      // iPhone Retina 3.5-inch
    }
    else fileName = [NSString stringWithFormat:@"%@-ipad", fileName]; // iPad
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:fileName];
    return textureAtlas;
}

@end
