//
//  EnterGameScreen.h
//  Trash Pack
//
//  Created by Beau Young on 24/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "TrashPackMasterScene.h"


@interface EnterGameScreen : TrashPackMasterScene

@property BOOL contentCreated;



@end
