//
//  MyScene.h
//  Trash Pack
//

//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "TrashPackMasterScene.h"

@interface SplashScreen : SKScene

@property BOOL contentCreated;

@end
