//
//  TVAdvertsScene.h
//  Trash Pack
//
//  Created by Beau Young on 30/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "TrashPackMasterScene.h"


@interface TVAdvertsScene : TrashPackMasterScene // Navigation Bar is handled in superclass

@property BOOL contentCreated;

@end
