//
//  AppDelegate.h
//  Trash Pack
//
//  Created by Beau Young on 24/10/2013.
//  Copyright (c) 2013 SharpAgency. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
